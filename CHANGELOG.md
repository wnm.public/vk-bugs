# Список изменений

## 1.5.3

* Убран баг с пикселем, обновлен Nuxt

## 1.5.2

* Обновлены зависимости, добавлен баг с пикселем

## 1.5.1

* Добавлены баги с загрузкой файла по абсолютному пути и base64

## 1.5.0

* Добавлена возможность разработчику указывать шаблон для кнопок
* Добавлена возможность протестировать фикс отдельно
* Баг с файлами перемещен в исправленные
* Обновлена ссылка на репозиторий в Gitlab
* Обновлены зависимости

## 1.4.0

* Исправления по верстке и UX
* Фикс запоминания тёмной темы, основанного на VK Storage
* Обновлены зависимости, новая версия VK API

## 1.3.5

* Добавлен новый тип бага для тестов с использованием новой функции внутренней библиотеки

## 1.3.4

* Состояния предыдущих тестов больше не теряются при переходе между страницами

## 1.3.3

* Убран ts-ignore, благодаря фиксу vk-bridge

## 1.3.2

* Теперь шеры на стену и в истории не публикуются сразу в общем доступе
  * Истории публикуются в личные сообщения автора истории
  * Записи уходят в отложку на месяц

## 1.3.1

* Обновлены зависимости

## 1.3.0

* Использование / как главную страницу вместо /content из-за непредвиденных багов
* Использование отдельной страницы для бага SSRTokenGet
* Продолжение рефакторинга Vuex: использование accessor (nuxt-typed-vuex)
* Для ошибок с состоянием по-умолчанию используется дефолтная иконка
* Обновлены зависимости

## 1.2.0

* Улучшен UI и UX, исправлены ошибки

## 1.1.0

* Добавлен новый баг
* Улучшен UI и UX
* Начат лог версий, текущая версия добавлена в шапку

## 1.0.0

* Первый релиз
