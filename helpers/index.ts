import got from 'got';

const FormData = require('form-data');

export function uploadStories(server: string, file: Buffer, video = false): Promise<{ [key: string]: any }> {
  const formData = new FormData();
  formData.append(video ? 'video_file' : 'file', file, {
    filename: video ? 'video.mp4' : 'photo.png',
  });

  return got(server, {
    method: 'POST',
    body: formData,
  }).json<{ [key: string]: any }>();
}
