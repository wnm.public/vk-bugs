export interface Bug {
  readonly title: string
  readonly description: string
  readonly resolved: boolean
  readonly button?: {
    title?: string
    attributes: { [key: string]: any } | true
    action: (() => any) | true
  }
  testOfFixAvailable?: boolean
  message?: string
  error?: string
}

export interface BugArray extends Bug {
  readonly type: BugsTypes
}

export enum BugsTypes {
  'StoryUpload' = 1,
  'SSRTokenGet',
  'AndroidDownload',
  'ShareResultString',
  'StorySave',
  'Base64MobileDownload',
  'SlashMobileDownload'
}

export type Bugs = Record<BugsTypes, Bug>
export type BugsArray = Array<BugArray>
export type Themes = 'dark' | 'light' | 'default'
