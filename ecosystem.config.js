module.exports = {
  apps: [{
    name: 'frontend',
    script: 'npm',
    args: 'start',
    error_file: '/var/log/pm2/frontend/error.log',
    out_file: '/var/log/pm2/frontend/out.log',
    time: true,
  }],
}
