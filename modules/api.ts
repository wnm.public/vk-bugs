import { Module } from '@nuxt/types';

interface Options {
}

const session: Module<Options> = function (moduleOptions) {
  if (this.options.dev || this.options._start) {
    this.addServerMiddleware('~/api/index');
  }
};

export default session;
