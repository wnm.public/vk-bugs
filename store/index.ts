import { actionTree, getAccessorType, getterTree, mutationTree } from 'typed-vuex';
import { DefaultUpdateConfigData } from '@vkontakte/vk-bridge';
import MobileDetect from 'mobile-detect';
import { Bugs, BugsArray, BugsTypes, Themes } from '~/types';

export const strict = false;

export const state = () => ({
  appId: 0,
  userId: 0,
  server: '/',
  isMobile: false as boolean,
  isAndroid: false as boolean,
  bugs: {} as Bugs,
  bugsArray: [] as BugsArray,
  loadedBugs: false,
  theme: 'default' as Themes,
});

export const getters = getterTree(state, {});

export const mutations = mutationTree(state, {
  SET_THEME(state, val: Themes) {
    state.theme = val;
  },
  SET_SERVER(state, server: string) {
    state.server = server;
  },
  SET_VK_DATA(state, { appId, userId }: { appId?: number, userId?: number }) {
    if (appId)
      state.appId = appId;

    if (userId)
      state.userId = userId;
  },
  UPDATE_BUGS_ARRAY(state) {
    state.bugsArray = Object.entries(state.bugs).map(([key, bug]) => {
      return {
        type: (parseInt(key) as BugsTypes),
        ...bug,
      };
    });
  },
});

export const actions = actionTree(
  { state, getters, mutations },
  {
    nuxtServerInit({ state }, { req }) {
      const md = new MobileDetect(req.headers['user-agent']);
      state.isMobile = !!md.mobile();
      state.isAndroid = md.is('AndroidOS');
    },
    async VKWebAppUpdateConfig({ state }, data: DefaultUpdateConfigData) {
      if (!this.app.vuetify) return; //так не бывает
      let isDark = data.scheme === 'client_dark' || data.scheme === 'space_gray';

      const storageResult = await this.app.$wnm.vkStorage.get('theme');
      if (storageResult) {
        isDark = storageResult === 'dark';
        state.theme = storageResult === 'dark' ? 'dark' : 'light';
      }

      this.app.vuetify.framework.theme.dark = isDark;
    },
    async loadBugs({ state, commit }) {
      if (state.loadedBugs) return;

      state.bugs = (await this.app.$functions.srequest<{ bugs: Bugs }>('bugs')).bugs;
      commit('UPDATE_BUGS_ARRAY');
      state.loadedBugs = true;
    },
  });

export const accessorType = getAccessorType({
  state,
  getters,
  mutations,
  actions,
});
