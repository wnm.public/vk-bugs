import bodyParser from 'body-parser'
import WNMFunctions from '@wnm.development/functions'
import ru from 'vuetify/es5/locale/ru'

const fs = require('fs')

const functions = new WNMFunctions()

export default {
  ssr: true,
  globalName: 'bugs',
  head: {
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, viewport-fit=cover',
      },
      {
        'http-equiv': 'cache-control',
        hid: 'http-equiv-1',
        content: 'max-age=0',
      },
      {
        'http-equiv': 'cache-control',
        hid: 'http-equiv-2',
        content: 'no-store',
      },
      {
        'http-equiv': 'expires',
        hid: 'http-equiv-3',
        content: '-1',
      },
      {
        'http-equiv': 'expires',
        hid: 'http-equiv-4',
        content: 'Tue, 01 Jan 1980 1:00:00 GMT',
      },
      {
        'http-equiv': 'pragma',
        hid: 'http-equiv-5',
        content: 'no-cache',
      },
    ],
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons',
      },
    ],
  },
  loading: { color: '#000' },
  css: [],
  server: {
    port: 8080,
    host: '0.0.0.0',

    https: process.env.NODE_ENV === 'development' ? {
      key: fs.readFileSync(functions.getCerts().key),
      cert: fs.readFileSync(functions.getCerts().cert),
    } : false,
    //https: false
  },
  plugins: [
    '~/plugins/global', '~/plugins/bugsTypes.mixin', {
      src: '~/plugins/client',
      mode: 'client',
    },
  ],
  buildModules: [
    ['@nuxt/typescript-build', {
      typeCheck: true,
    }],
    'nuxt-typed-vuex',
    ['@nuxtjs/vuetify', {
      lang: {
        locales: { ru },
        current: 'ru',
      },
      icons: {
        iconfont: 'mdi',
      },
      theme: {
        dark: true,
        themes: {
          light: {
            primary: '#5C9CE6',
            success: '#4BB34B',
            error: '#FF3347',
            secondary: '#1F1B2C',
            purple: '#792EC0',

            accent: '#74A2D6',
            info: '#346CAD',
            warning: '#FFA000',
          },
          dark: {
            primary: '#5C9CE6',
            success: '#4BB34B',
            error: '#FF3347',
            secondary: '#1F1B2C',
            purple: '#792EC0',

            accent: '#74A2D6',
            info: '#346CAD',
            warning: '#FFA000',
          },
        },
      },
    }],
  ],
  modules: ['~/modules/api'],
  serverMiddleware: [bodyParser.json()],
  build: {
    devtools: process.env.NODE_ENV === 'development',
    publicPath: '/static/',
    extractCSS: process.env.NODE_ENV !== 'development',
    transpile: [
      'typed-vuex',
    ],
    extend(config, ctx) {
    },
  },
}
