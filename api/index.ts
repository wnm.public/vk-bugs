import { NextFunction, Request, Response } from 'express';
import { uploadStories } from './../helpers';
import { readFileSync } from 'fs';
import { Bugs, BugsTypes } from '../types';

const bugs: Bugs = {
  [BugsTypes.StoryUpload]: {
    title: 'Получение сервера Stories (Android)',
    description: 'Получение сервера Stories не грузило историю, вместо этого возвращало какую-то строку. Обходится костылем в виде execute<br><br>При нажатии на "протестировать" сторис опубликуется у вас в личных сообщениях (не в новостях). <br><br><b>Проблема "решена" в версии API 5.118</b>',
    resolved: true,
  },
  [BugsTypes.SSRTokenGet]: {
    title: 'Получение токена на странице, кроме главной (моб. устройства)',
    description: 'Токен не получался на странице, если она не равна главной. Баг применим для роутинга вида /, /somepage и пр. (не <code>hash mode</code>)',
    resolved: true,
  },
  [BugsTypes.AndroidDownload]: {
    title: 'Скачивание файла (Android)',
    description: 'Скачивание файла на Android не работало по прямой ссылке<br><br><b>Проблема решена поддержкой <code>VKWebAppDownloadFile</code> на Android</b>',
    resolved: true,
    button: {
      title: 'Скачать',
      attributes: true,
      action: true,
    },
    testOfFixAvailable: true,
  },
  [BugsTypes.ShareResultString]: {
    title: 'Ответ строкой после шера на стену (как минимум Android)',
    description: 'После шера на стену post_id иногда возвращался как строка, а не как число.<br><br>При нажатии на "протестировать" вам будет предложено опубликовать запись, она будет добавлена в отложенные и опубликована через месяц, если вы её не удалите',
    resolved: false,
  },
  [BugsTypes.StorySave]: {
    title: 'Не работающий stories.save',
    description: 'После передачи в stories.save <code>upload_results</code> он возвращал пустой <code>items</code>. Повторно отправить уже было нельзя, якобы сторис уже была загружена.<br><br>При нажатии на "протестировать" сторис опубликуется у вас в личных сообщениях (не в новостях).',
    resolved: true,
  },
  [BugsTypes.SlashMobileDownload]: {
    title: 'Загрузка файлов по абсолютному пути',
    description: 'Загрузка файлов по пути, например, /images/test123.png, не работает с методом <code>VKWebAppDownloadFile</code>',
    resolved: false,
  },
  [BugsTypes.Base64MobileDownload]: {
    title: 'Загрузка файлов base64',
    description: 'Загрузка файлов base64, например, <code>iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==</code> не работает с методом <code>VKWebAppDownloadFile</code>',
    resolved: false,
  }
};

function jsonResponse(response: { [key: string]: any }, res: Response) {
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(response));
}

export default async function (req: Request, res: Response, next: NextFunction) {
  if (!req) return;

  res.setHeader('Cache-Control', 'no-cache');
  const path = req.url.split('/').slice(1, req.url.split('/').length);

  if (path[0] === 'api') {
    if (path[1] === 'bugs') {
      jsonResponse({ bugs }, res);
    }
    if (path[1] === 'StoryUpload') {
      const result = await uploadStories(req.body.server, readFileSync(__dirname + '/assets/story.png'));
      jsonResponse({
        result,
      }, res);
    }
    if (path[1] === 'StorySave') {
      const result = await uploadStories(req.body.server, readFileSync(__dirname + '/assets/story.png'));
      jsonResponse({
        result: result.response.upload_result,
      }, res);
    }
  }

  if (!res.headersSent)
    next();
}
