import Vue from 'vue';
import { Plugin } from '@nuxt/types';
import { BugsTypes } from '../types';

declare module '@nuxt/types' {
  interface Context {
    bugsTypes: typeof BugsTypes
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    bugsTypes: typeof BugsTypes
  }
}


const bugsTypesMixin: Plugin = (context) => {
  context.bugsTypes = BugsTypes;
  Vue.prototype.bugsTypes = BugsTypes;
};

export default bugsTypesMixin;
