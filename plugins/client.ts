import Vue from 'vue';
import WNM from '@wnm.development/vue';
import { Plugin } from '@nuxt/types';

const plugin: Plugin = ({ store, app }) => {
  Vue.use(WNM, {
    vue: Vue,
    nuxtApp: app,
    store,
    connectAuthOnInit: false,
  });
};


export default plugin;

