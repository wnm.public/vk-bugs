import Vue from 'vue';
import { Plugin } from '@nuxt/types';
import WNMFunctions from '@wnm.development/functions';
import { Framework } from 'vuetify';

declare module 'vue/types/vue' {
  interface Vue {
    $vuetify: Framework
  }
}

const main: Plugin = ({ app, store }) => {
  new WNMFunctions({
    nuxtApp: app,
    vue: Vue,
    useBearer: false,
    vuex: store,
  });
};

export default main;
